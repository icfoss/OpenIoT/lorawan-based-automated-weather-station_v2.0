# Automatic Weather Station

LoRaWAN based Automatic weather station is a system which measures the environmental parameters and transmits the data at regular intervals using LoRaWAN Technology. Automated weather stations (sometimes called automatic weather stations) provide an integrated system of components that are used to measure, monitor, and study the weather and climate. These stations are typically used in weather, meteorology, and mesonet applications.
![AWS_1](https://gitlab.com/jaison_j/lorawan-based-automated-weather-station_v2.0/-/raw/master/Images/aws_1.jpg)*AWS at Taraba, Odisha*

# List of Parameters recorded

- Temperature
- Humidity
- Atmospheric Pressure
- Rain measurement
- Wind speed
- Wind Direction

LoRaWAN transmission is every 15 minutes.
The application code is written on top of the I-cube-lrwan package by STM32. 

# Prerequisites
  - STM32 Cube IDE [Tested]
  - [C1_dev_v1.0](https://gitlab.com/icfoss/OpenIoT/c1_dev_v1.0)
  - Solar Charge Controller
  - Solar Panel (3V, 6V voc)
  - Atmospheric Pressure Sensor - [(EMC-BARO-MAX-PIZ-16)](https://gitlab.com/jaison_j/lorawan-based-automated-weather-station_v2.0/-/blob/master/Hardware/Datasheets/ATMOSPHERIC%20PRESSURE.pdf)
  - Temperature/Humidity [(EMC-AT-RH/NTC-1/22(-20-60)](https://gitlab.com/jaison_j/lorawan-based-automated-weather-station_v2.0/-/blob/master/Hardware/Datasheets/AT-RH.pdf)
  - Wind Direction sensor [(EMC-WD-HES/20)](https://gitlab.com/jaison_j/lorawan-based-automated-weather-station_v2.0/-/blob/master/Hardware/Datasheets/WIND%20DIRECTION.pdf)
  - Wind Velocity sensor [(EMC-WS-8P/20)](https://gitlab.com/jaison_j/lorawan-based-automated-weather-station_v2.0/-/blob/master/Hardware/Datasheets/Wind%20Velocity%20CUP.pdf)
  - Raing guage Davis

# Getting Started
  - Make sure that you have a [C1_dev_v1.0](https://gitlab.com/icfoss/OpenIoT/c1_dev_v1.0) or any STM32 Board.
  - Connect the components as shown in the [Wiring Diagram](https://gitlab.com/jaison_j/lorawan-based-automated-weather-station_v2.0/-/blob/master/Hardware/Wiring_Diagram_AWS.pdf)
  - Install STM32 Cube IDE.
  - Select board : B-L072Z-LRWAN1
  - For programming connect C1_dev_v1.0 through B-L072Z-LRWAN1 [here](https://gitlab.com/jaison_j/lorawan-based-automated-weather-station_v2.0/-/blob/master/Hardware/Pgm_conection_STM32board.pdf).
  - For programming connect C1_dev_v1.0 through STM32 programmer Module [here](https://gitlab.com/jaison_j/lorawan-based-automated-weather-station_v2.0/-/blob/master/Hardware/Pgm_connection_STMProgrammer.pdf).
  - If using ABP(Activation by Personalisation) method, Change the address/keys of the device address, network session key, application session key.
  - If using OTAA (Over the Air Activation) method, Change the address/keys of the DUI (device EUI), application key.

# Contributing
Instructions coming up soon.

# License
This project is licensed under the MIT License - see the LICENSE.md file for details
